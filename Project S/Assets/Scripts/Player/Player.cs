﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fighter;

namespace projectS
{
	public class Player  {
        public Fighter.Fighter[] Fighters { get; private set; }

        public Fighter.Fighter CurrentFighter { get { return this.Fighters[currentFighterIndex]; } }

        public int currentFighterIndex = 0;

        public Player(Fighter.Fighter[] fighters)
        {
            this.Fighters = new Fighter.Fighter[fighters.Length];
            for(int i=0; i<fighters.Length; i++)
            {
                this.Fighters[i] = new Fighter.Fighter(fighters[i]);
            }
        }

        public void SelectFighter(int index)
        {
            if(this.Fighters[index].PersonalInfo.Health > 0)
            {
                currentFighterIndex = index;
            }
        }
	}
}
