﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Fighter;
namespace projectS
{
    public class FighterInfoView : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI name = null;
        [SerializeField]
        TextMeshProUGUI level = null;
        [SerializeField]
        TextMeshProUGUI hp = null;
        [SerializeField]
        TextMeshProUGUI experience = null;

        [SerializeField]
        AttackButtonView[] attackButtons;

        [SerializeField]
        Button[] selectFighterButtons;

        [SerializeField]
        Image front;
        [SerializeField]
        Image back;


        [SerializeField]
        Image healthBar;

        [SerializeField]
        Image expBar;
        

        [SerializeField]
        ImageDB imageDB;

        public void ShowInfo(Player player, Player opponent)
        {
            Fighter.Fighter fighter = player.CurrentFighter;
            name.text = fighter.PersonalInfo.FighterShowName;
            level.text = "Lv. " + fighter.PersonalInfo.Level.ToString();
            hp.text = fighter.PersonalInfo.Health.ToString() + "/" + fighter.BaseInfo.Health.ToString();
            healthBar.fillAmount = (float)fighter.PersonalInfo.Health / (float)fighter.BaseInfo.Health;
            experience.text = fighter.PersonalInfo.Experience.ToString();

            for(int i=0; i<4; i++)
            {
                if (fighter.PersonalInfo.Attacks.Length > i)
                {
                    attackButtons[i].Draw(fighter.PersonalInfo.Attacks[i].Info, fighter.BaseInfo.Attacks[i].Info, opponent.CurrentFighter);
                    attackButtons[i].transform.parent.gameObject.SetActive(true);
                }
                else
                {
                    attackButtons[i].transform.parent.gameObject.SetActive(false);
                }
            }
            
            for (int i = 0; i < player.Fighters.Length; i++)
            {
                selectFighterButtons[i].interactable = (player.Fighters[i].PersonalInfo.Health > 0);
            }


            front.sprite = imageDB.GetSprites(fighter.BaseInfo.FighterIDName).GetFront();
            back.sprite = imageDB.GetSprites(fighter.BaseInfo.FighterIDName).GetBack();

        }
    }
}

