﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace projectS
{
    [System.Serializable]
    public class ImageSettings
    {
        [SerializeField]
        string name = "name";
        [SerializeField]
        Sprite front = null;
        [SerializeField]
        Sprite back = null;


        public string GetName()
        {
            return name;
        }
        public Sprite GetFront()
        {
            return front;
        }
        public Sprite GetBack()
        {
            return back;
        }
    }


    public class ImageDB : MonoBehaviour
    {
        [SerializeField]
        ImageSettings[] imageSettings;

        Dictionary<string, ImageSettings> spriteDB;

        public void Init()
        {
            spriteDB = new Dictionary<string, ImageSettings>();
            foreach (var i in imageSettings)
            {
                spriteDB.Add(i.GetName(), i);
            }
        }

        public ImageSettings GetSprites(string name)
        {
            name = name.ToUpper();
            if(!spriteDB.ContainsKey(name))
            {
                foreach(var k in spriteDB.Keys)
                {
                    return spriteDB[k];
                }
            }
            return spriteDB[name];
        }
    }
}

