﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Fighter;

namespace projectS
{
    public class AttackButtonView : MonoBehaviour
    {

        [SerializeField]
        TextMeshProUGUI AttackNameText;

        [SerializeField]
        TextMeshProUGUI AttackTypeText;

        [SerializeField]
        TextMeshProUGUI AttackDescriptionText;

        [SerializeField]
        TextMeshProUGUI AccuracyText;

        [SerializeField]
        TextMeshProUGUI BaseDamageText;

        [SerializeField]
        TextMeshProUGUI PPLeftText;

        [SerializeField]
        TextMeshProUGUI TypeEffectivenessText;

        [SerializeField]
        TextMeshProUGUI EffectChanceText;


        [SerializeField]
        Image TypeColor;

        public void Draw(string attackName, string attackType, string attackDescription
            , int accuracy, int baseDamage, int ppLeft, int basePP,
            int typeEffectChance, Color typeColor, float effectiveness)
        {
            AttackNameText.text = attackName;
            AttackTypeText.text = attackType;
            AttackDescriptionText.text = attackDescription;
            AccuracyText.text = accuracy.ToString();
            BaseDamageText.text = baseDamage.ToString();
            PPLeftText.text = ppLeft + "/" + basePP;
            TypeEffectivenessText.text = effectiveness == 0 ? "NO EFFECT" : effectiveness < 1? "NOT EFFECTIVE " : effectiveness < 2 ? "EFFECTIVE" :"SUPER EFFECTIVE";
            TypeEffectivenessText.color = effectiveness == 0 ? new Color(0.6f, 0.1f,0.1f) : effectiveness < 1 ? new Color(1,0,0) : effectiveness < 2 ? new Color(0.4f, 0.4f, 0.4f) : new Color(0, 1, 0);
            EffectChanceText.text = typeEffectChance + "%";
            TypeColor.color = typeColor;
            Debug.Log("type color : " + typeColor.ToString());
        }


        public void Draw(AttackInfo current, AttackInfo baseInfo, Fighter.Fighter opponent)
        {
            float effectiveness = 1.0f;
            foreach(var t in opponent.PersonalInfo.Types)
            {
                effectiveness *= t.StrengthAgainst(current.ElementType);
            }

            Draw(current.AttackIDName, current.ElementType.TypeName, current.Description, current.Accuracy, current.Damage, current.PP, baseInfo.PP, current.AdditionalMoveEffectChance, current.ElementType.TypeColor, effectiveness);
        }

    }

}
