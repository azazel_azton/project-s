﻿using Parsers;
using Fighter;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace projectS
{
    [System.Serializable]
    public class InfoContainer
    {
        public Dictionary<string, FighterType> FighterTypes;
        public Dictionary<string, AttackInfo> AttackInfos;
        public Dictionary<string, FighterInfo> Fighters;
        public Dictionary<string, NatureInfo> Natures;


        void ReloadInfo()
        {
            FighterTypes = new Dictionary<string, FighterType>();
            Object[] typeObjects = Resources.LoadAll("ScriptableObjects/Types/");
            Debug.Log("fighter objects : " + typeObjects.Length);
            foreach(var f in typeObjects)
            {
                FighterType temp = (FighterType)f;
                FighterTypes.Add(temp.TypeName, temp);
            }
            AttackInfos = new Dictionary<string, AttackInfo>();
            Object[] attackObjects = Resources.LoadAll("ScriptableObjects/Attacks/");
            foreach (var f in attackObjects)
            {
                AttackInfo temp = (AttackInfo)f;
                AttackInfos.Add(temp.AttackIDName, temp);
            }
            Fighters = new Dictionary<string, FighterInfo>();
            Object[] fighterInfoObjects = Resources.LoadAll("ScriptableObjects/Fighters/");
            foreach (var f in fighterInfoObjects)
            {
                FighterInfo temp = (FighterInfo)f;
                Fighters.Add(temp.FighterIDName, temp);
            }
            Natures = new Dictionary<string, NatureInfo>();
            Object[] natureInfoObjects = Resources.LoadAll("ScriptableObjects/Natures/");
            foreach (var f in natureInfoObjects)
            {
                NatureInfo temp = (NatureInfo)f;
                Natures.Add(temp.Name, temp);
            }
        }

        public void LoadInfo(bool reload)
        {
            if(reload)
            {
                ReloadInfo();
                return;
            }
            Debug.Log("Start Loading Info");
            FighterTypes = new Dictionary<string, FighterType>();
            FighterType[] types = new TypesParser().ParseToFighterTypeArray(new StreamReader("Assets/TextFiles/types.txt").ReadToEnd());            
            foreach (var t in types)
            {
                if(t != null && t.TypeName != null && t.TypeName != "")
                {
                    if (!FighterTypes.ContainsKey(t.TypeName))
                        FighterTypes.Add(t.TypeName, t);
                    else
                        Debug.LogWarning("Trying to add a fighter element type with an already existing name");
                }
            }
            Debug.Log("Figher Types Loaded");

            AttackInfos = new Dictionary<string, AttackInfo>();
            AttackInfo[] attacks = new AttackParser().ParseToAttackInfoArray(new StreamReader("Assets/TextFiles/moves.txt").ReadToEnd(), this);
            foreach (var a in attacks)
            {
                if(a != null && a.AttackIDName != null && a.AttackIDName != "")
                {
                    if (!AttackInfos.ContainsKey(a.AttackIDName))
                        AttackInfos.Add(a.AttackIDName, a);
                    else
                        Debug.LogWarning("Trying to add an attack with an already existing name");
                }
            }
            Debug.Log("Attack Types Loaded");
            Fighters = new Dictionary<string, FighterInfo>();
            FighterInfo[] fighters = new FighterParser().ParseToFighterIntoArray(new StreamReader("Assets/TextFiles/fighters.txt").ReadToEnd(),this);
            foreach (var f in fighters)
            {
                if(f != null && f.FighterIDName != null && f.FighterIDName != "")
                {
                    if(!Fighters.ContainsKey(f.FighterIDName))
                        Fighters.Add(f.FighterIDName, f);
                    else
                        Debug.LogWarning("Trying to add a fighter with an already existing name");
                }
            }
            Debug.Log("Fighters Loaded");
            Natures = new Dictionary<string, NatureInfo>();
            NatureInfo[] natures = new NaturesParser().ParseToNatureIntoArray(new StreamReader("Assets/TextFiles/natures.txt").ReadToEnd(), this);
            foreach (var n in natures)
            {
                if (n != null && n.Name != null)
                {
                    if (!Natures.ContainsKey(n.Name))
                        Natures.Add(n.Name, n);
                    else
                        Debug.LogWarning("Trying to add a nature with an already existing name");
                }
            }
            Debug.Log("Done Loading Info");
            

        }

    }
}

