﻿using System.Collections.Generic;
using UnityEngine;

namespace Fighter
{
    //Element type
    public class FighterType : ScriptableObject
    {
        public string TypeName;
        public string[] Weaknesses;//Attack from this type hurts more
        public string[] Resistances;//Attack from this type hurts less
        public string[] Immunities;//Attack from this type does nothing
        public Color TypeColor = new Color(0.8f, 0.8f, 0.8f, 1);

        public void SetFighterType(string name, string[] weaknesses, string[] resistances, string[] immunities)
        {
            this.TypeName = name;
            this.Weaknesses = weaknesses;
            this.Resistances = resistances;
            this.Immunities = immunities;
        }

        public FighterType(string name, string[] weaknesses, string[] resistances, string[] immunities)
        {
            this.TypeName = name;
            this.Weaknesses = weaknesses;
            this.Resistances = resistances;
            this.Immunities = immunities;
        }

        public FighterType(FighterType copy)
        {
            this.TypeName = copy.TypeName;
            this.Weaknesses = copy.Weaknesses;
            this.Resistances = copy.Resistances;
            this.Immunities = copy.Immunities;
        }

        public float StrengthAgainst(FighterType attack)
        {
            foreach(var s in Weaknesses)
            {
                if(attack.TypeName == s)
                {
                    return 2;
                }
            }
            foreach (var s in Resistances)
            {
                if (attack.TypeName == s)
                {
                    return 0.5f;
                }
            }
            foreach (var s in Immunities)
            {
                if (attack.TypeName == s)
                {
                    return 0;
                }
            }
            return 1;
        }
    }
}
