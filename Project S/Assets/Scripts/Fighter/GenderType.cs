﻿namespace Fighter
{
    public class GenderType
    {
        public float PercentageMale { get; private set; }
        public float PercentageFemale { get { return 100 - PercentageMale; } }
        
        public GenderType(float percentageMale)
        {
            this.PercentageMale = percentageMale;
        }

        public GenderType(GenderType copy)
        {
            this.PercentageMale = copy.PercentageMale;
        }

    }
}