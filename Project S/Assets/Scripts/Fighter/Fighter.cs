﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Fighter {
    [System.Serializable]
    public class Fighter
    {
        public FighterInfo BaseInfo { get; private set; }
        public FighterInfo PersonalInfo { get; private set; }

        public Fighter(Fighter copy)
        {
            this.BaseInfo = new FighterInfo(copy.BaseInfo);
            this.PersonalInfo = new FighterInfo(copy.PersonalInfo);
        }

        public Fighter(FighterInfo baseInfo)
        {
            this.BaseInfo = baseInfo;
            this.PersonalInfo = new FighterInfo(baseInfo, false);
        }

        public void SetNature(NatureInfo natureInfo)
        {
            this.PersonalInfo.Nature = natureInfo;
        }
    }
}


