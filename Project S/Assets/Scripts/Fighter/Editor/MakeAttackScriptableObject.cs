﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using projectS;

public class MakeAttackScriptableObject
{
    [MenuItem("Assets/Create/My Scriptable Object")]
    public static void CreateMyAsset()
    {
        DualerScriptableObject asset = ScriptableObject.CreateInstance<DualerScriptableObject>();

        AssetDatabase.CreateAsset(asset, "Assets/NewScripableObject.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}