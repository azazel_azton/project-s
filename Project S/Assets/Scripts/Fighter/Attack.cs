﻿using UnityEngine;

namespace Fighter
{
    [System.Serializable]
    public class Attack
    {
        public AttackInfo Info;
        public int UnlockLevel;

        public Attack(AttackInfo info, int unlockLevel)
        {
            this.Info = info;
            this.UnlockLevel = unlockLevel;
        }

        public Attack(Attack copy)
        {
            this.Info = ScriptableObject.CreateInstance<AttackInfo>();
            this.Info.SetAttackInfo(copy.Info.AttackIDName, copy.Info.Priority, copy.Info.Damage, copy.Info.Accuracy, copy.Info.PP, copy.Info.AdditionalMoveEffectChance, copy.Info.ElementType, copy.Info.AdditionalMoveEffect, copy.Info.TargetingType, copy.Info.MoveFlags, copy.Info.Description, copy.Info.MoveType);
            
            this.UnlockLevel = copy.UnlockLevel;
        }

    }
}

