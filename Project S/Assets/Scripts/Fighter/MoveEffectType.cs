﻿namespace Fighter
{
	public class MoveEffectType {
        		
		public string EffectName { get; private set; }

		public MoveEffectType(string name)
		{
			this.EffectName = name;
		}

		public MoveEffectType(MoveEffectType copy)
		{
			this.EffectName = copy.EffectName;
		}
	}
}
