﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fighter
{
    [System.Serializable]
    public class StatusInfoObject
    {
        [SerializeField]
        public enum Type
        {
            UNSET,
            MinTurns,
            MaxTurns,
            DamagePerTurn,
            DamageIncreasePerTurn,
            MaxDamagePerTurn,
            DamagePerTurnPercentage,
            DamageIncreasePerTurnPercentage,
            MaxDamagePerTurnPercentage,
            HealPerTurn,
            HealIncreasePerTurn,
            HealDamagePerTurn,
            HealPerTurnPercentage,
            HealIncreasePerTurnPercentage,
            MaxHealPerTurnPercentage,
            ChanceOfAttackSucces,
            ChanceOfAttackFail,
            NoAttackPossible,
            NoSwitchPossible,
            EnemyNoAttackPossible,
            EnemyNoSwitchPossible,
            EnemiesNoAttackPossible,
            EnemiesNoSwitchPossible,
            BacklashChance,
            BacklashDamage,
            BacklashDamagePercentage,
            CanNotUseItem,
            CanNotUseItemType
        }

        [SerializeField]
        public Type ObjectType = Type.UNSET;

        [SerializeField]
        public bool IsBool = false;

        [SerializeField]
        public bool IsInt = false;

        [SerializeField]
        public bool IsFloat = false;

        [SerializeField]
        public bool BoolValue;

        [SerializeField]
        public float FloatValue;

        [SerializeField]
        public int IntValue;

    }
}
