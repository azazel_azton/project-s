﻿using UnityEngine;
using System.Collections;
using UnityEditor;


namespace Fighter
{
    [CustomEditor(typeof(StatusInfo))]
    public class StatusInfoEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            StatusInfo myTarget = (StatusInfo)target;
            GUILayout.BeginVertical();

            if (myTarget.Effects != null)
            {
                int ctr = 0;
                foreach(var e in myTarget.Effects)
                {
                    EditorGUILayout.LabelField("-------------------------------------------------------------------------------------------------");
                    EditorGUILayout.LabelField("effect: ");

                    e.ObjectType = (StatusInfoObject.Type)EditorGUILayout.EnumPopup("Type:", e.ObjectType);

                    if (e.IsBool)
                    {
                        e.BoolValue = EditorGUILayout.Toggle("True/False:", e.BoolValue);
                        if (GUILayout.Button("return to choice of value"))
                        {
                            e.IsBool = false;
                        }
                    }
                    else if (e.IsInt)
                    {
                        e.IntValue = EditorGUILayout.IntField("Integer Value:", e.IntValue);
                        if (GUILayout.Button("return to choice of value"))
                        {
                            e.IsInt = false;
                        }
                    }
                    else if (e.IsFloat)
                    {
                        e.FloatValue = EditorGUILayout.FloatField("Float Value", e.FloatValue);
                        if (GUILayout.Button("return to choice of value"))
                        {
                            e.IsFloat = false;
                        }
                    }
                    else
                    {
                        e.IsBool = EditorGUILayout.Toggle("Value is a boolean (true or false):", e.IsBool);
                        e.IsInt = EditorGUILayout.Toggle("Value is an integer (round number):", e.IsInt);
                        e.IsFloat = EditorGUILayout.Toggle("Value is a float (non round number):", e.IsFloat);
                    }
                    if (GUILayout.Button("delete this effect"))
                    {
                        myTarget.Effects.RemoveAt(ctr);
                        goto exitLoop;
                    }
                    EditorGUILayout.LabelField("-------------------------------------------------------------------------------------------------");

                    ctr++;
                }
            }

            exitLoop:

            if (GUILayout.Button("add effect"))
            {
                if(myTarget.Effects == null)
                {
                    myTarget.Effects = new System.Collections.Generic.List<StatusInfoObject>();
                }
                myTarget.Effects.Add(new StatusInfoObject());
            }
            GUILayout.EndHorizontal();

            EditorUtility.SetDirty(myTarget);
        }
    }
}