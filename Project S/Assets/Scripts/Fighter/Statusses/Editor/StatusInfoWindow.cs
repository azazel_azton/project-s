﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Fighter
{
    class StatusInfoWindow : EditorWindow
    {
        [MenuItem("Window/Project S/StatusWindow")]

        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(StatusInfoWindow));
        }

        void OnGUI()
        {
            // The actual window code goes here
            GUILayout.Label("WARNING: Rename your scriptable object, this will overwrite!");
            if (GUILayout.Button("Create new status object"))
            {
                StatusInfo asset = ScriptableObject.CreateInstance<StatusInfo>();
                AssetDatabase.CreateAsset(asset, "Assets/Resources/ScriptableObjects/StatusObjects/" + "temporaryName" + ".asset");
                AssetDatabase.SaveAssets();
            }
            GUILayout.Label("Will be created in : " + "Assets/Resources/ScriptableObjects/StatusObjects/" + " , folder");
        }
    }
}

