﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fighter
{
    public class StatusInfo : ScriptableObject
    {

        public List<StatusInfoObject> Effects;
    }

}
