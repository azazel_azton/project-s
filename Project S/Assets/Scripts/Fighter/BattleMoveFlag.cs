﻿namespace Fighter
{
    public class BattleMoveFlag
    {
        public string ID { get; private set; }

        public BattleMoveFlag(string id)
        {
            this.ID = id;
        }
        public BattleMoveFlag(BattleMoveFlag copy)
        {
            this.ID = copy.ID;
        }
    }
}

