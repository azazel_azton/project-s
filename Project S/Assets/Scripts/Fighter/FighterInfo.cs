﻿using System;
using UnityEngine;

namespace Fighter
{
    [System.Serializable]
    public class FighterInfo : ScriptableObject
    {
        private FighterInfo baseInfo;

        public string FighterIDName;

        public string FighterShowName;

        public int Level;

        public Attack[] Attacks;

        public FighterType[] Types;

        public int Health;

        public int Attack;

        public int Defense;

        public int SPAttack;

        public int SPDefense;

        public int Speed;

        public int Experience;

        public MoveEffectType StatusCondition;

        public bool IsShiny;

        public Sprite Front;

        public Sprite Back;

        public NatureInfo Nature;

        public void SetFighterInfo(string idName, string showName, Attack[] attacks, FighterType[] types, int health, int attack, int defense, int spAttack, int spDefense, int speed, int experience, int level = 1)
        {
            this.FighterIDName = idName;
            this.FighterShowName = showName;
            this.Health = health;
            this.Attack = attack;
            this.Defense = defense;
            this.SPAttack = spAttack;
            this.SPDefense = spDefense;
            this.Speed = speed;
            this.Experience = experience;
            this.Level = level;

            this.Attacks = new Attack[attacks.Length];
            for (int attackIndex = 0; attackIndex < attacks.Length; attackIndex++)
            {
                if (attacks[attackIndex] != null)
                    this.Attacks[attackIndex] = attacks[attackIndex];
            }
            this.Types = new FighterType[types.Length];
            for (int typeIndex = 0; typeIndex < types.Length; typeIndex++)
            {
                if (types[typeIndex] != null)
                    this.Types[typeIndex] = types[typeIndex];
            }
        }

        public FighterInfo(string idName, string showName, Attack[] attacks, FighterType[] types, int health, int attack, int defense, int spAttack, int spDefense, int speed, int experience, int level = 1)
        {
            this.FighterIDName = idName;
            this.FighterShowName = showName;
            this.Health = health;
            this.Attack = attack;
            this.Defense = defense;
            this.SPAttack = spAttack;
            this.SPDefense = spDefense;
            this.Speed = speed;
            this.Experience = experience;
            this.Level = level;

            this.Attacks = new Attack[attacks.Length];
            for (int attackIndex = 0; attackIndex < attacks.Length; attackIndex++)
            {
                if (attacks[attackIndex] != null)
                    this.Attacks[attackIndex] = attacks[attackIndex];
            }
            this.Types = new FighterType[types.Length];
            for (int typeIndex = 0; typeIndex < types.Length; typeIndex++)
            {
                if (types[typeIndex] != null)
                    this.Types[typeIndex] = types[typeIndex];
            }
        }

        public FighterInfo(FighterInfo copy, bool useAssets = true)
        {
            this.FighterIDName = copy.FighterIDName;
            this.FighterShowName = copy.FighterShowName;
            this.Health = copy.Health;
            this.Attack = copy.Attack;
            this.Defense = copy.Defense;
            this.SPAttack = copy.SPAttack;
            this.SPDefense = copy.SPDefense;
            this.Speed = copy.Speed;
            this.Experience = copy.Experience;
            this.Level = copy.Level;
            this.Nature = copy.Nature;
            this.Attacks = new Attack[copy.Attacks.Length];
            for (int attackIndex = 0; attackIndex < copy.Attacks.Length; attackIndex++)
            {
                if (copy.Attacks[attackIndex] != null)
                    this.Attacks[attackIndex] = useAssets? copy.Attacks[attackIndex] : new Attack(copy.Attacks[attackIndex]);
            }
            this.Types = new FighterType[copy.Types.Length];
            for (int typeIndex = 0; typeIndex < copy.Types.Length; typeIndex++)
            {
                if (copy.Types[typeIndex] != null)
                    this.Types[typeIndex] = useAssets ? copy.Types[typeIndex] : new FighterType(copy.Types[typeIndex]);
            }
        }

        public void DecreaseHealth(int damage)
        {
            Health -= damage;
            if (Health < 0)
                Health = 0;
        }
        
        public int UseValue(int baseVal, NatureInfo.Type natureType)
        {
            if(Nature == null)
            {
                Debug.LogError("Forgot to set Nature for this Fighter");
            }
            float nature = Nature.Beneficial == natureType ? 1.1f : Nature.Hindering == natureType ? 0.9f : 1.0f;            

            return Mathf.RoundToInt(((((2.0f * (float)baseVal + 94.0f) * (float)Level) / 100.0f) + 5.0f) * nature);
        }
    }
}

