﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Fighter
{
    [System.Serializable]
    public class NatureInfo : ScriptableObject
    {
        public enum Type { None, Attack, Defense, Speed, SpAttack, SpDefense };

        public string Name;

        public Type Beneficial;

        public Type Hindering;

        public void SetNatureInfo(string name, Type beneficial, Type hindering)
        {
            this.Name = name;
            this.Beneficial = beneficial;
            this.Hindering = hindering;
        }

        public NatureInfo(string name, Type beneficial, Type hindering)
        {
            this.Name = name;
            this.Beneficial = beneficial;
            this.Hindering = hindering;
        }


        public NatureInfo(NatureInfo copy)
        {
            this.Name = copy.Name;
            this.Beneficial = copy.Beneficial;
            this.Hindering = copy.Hindering;
        }

    }

}
