﻿using System;
using UnityEngine;

namespace Fighter
{
    public class AttackInfo : ScriptableObject
    {
        public string AttackIDName; //Base Attack ID 

        public int Priority; //Base Priority

        public int Damage; //Base Damage

        public int Accuracy;//Base Accuracy

        public int PP; //Base PP

        public int AdditionalMoveEffectChance; //Base additional move effect chance, 0 - 100

        public FighterType ElementType; //ElementType

        public MoveEffectType AdditionalMoveEffect; //Base additional move effect

        public TargetingType TargetingType; //Base targetting type

        public BattleMoveFlag[] MoveFlags; //Base move flags

        public string Description; //The attacks description

        public string MoveType; //The attacks description

        public void SetAttackInfo(string idName, int priority, int damage, int accuracy, int pp,
            int additionalEffectChance, FighterType elementType, MoveEffectType additionalEffectType,
            TargetingType targetingType, BattleMoveFlag[] moveFlags, string description, string moveType)
        {
            this.AttackIDName = idName;
            this.Priority = priority;
            this.Damage = damage;
            this.Accuracy = accuracy;
            this.PP = pp;
            this.AdditionalMoveEffectChance = additionalEffectChance;
            this.ElementType = elementType;
            this.AdditionalMoveEffect = additionalEffectType;
            this.TargetingType = targetingType;
            this.MoveFlags = moveFlags;
            this.Description = description;
            this.MoveType = moveType;
        }

        public AttackInfo(string idName, int priority, int damage, int accuracy, int pp, 
            int additionalEffectChance,FighterType elementType, MoveEffectType additionalEffectType, 
            TargetingType targetingType, BattleMoveFlag[] moveFlags, string description, string moveType)
        {
            this.AttackIDName = idName;
            this.Priority = priority;
            this.Damage = damage;
            this.Accuracy = accuracy;
            this.PP = pp;
            this.AdditionalMoveEffectChance = additionalEffectChance;
            this.ElementType = elementType;
            this.AdditionalMoveEffect = additionalEffectType;
            this.TargetingType = targetingType;
            this.MoveFlags = moveFlags;
            this.Description = description;
            this.MoveType = moveType;
        }

        public AttackInfo(AttackInfo copy)
        {
            this.AttackIDName = copy.AttackIDName;
            this.Priority = copy.Priority;
            this.Damage = copy.Damage;
            this.Accuracy = copy.Accuracy;
            this.PP = copy.PP;
            this.AdditionalMoveEffectChance = copy.AdditionalMoveEffectChance;
            this.ElementType = copy.ElementType;
            this.AdditionalMoveEffect = copy.AdditionalMoveEffect;
            this.TargetingType = copy.TargetingType;              
            this.MoveFlags = copy.MoveFlags;
            this.Description = copy.Description;
            this.MoveType = copy.MoveType;
        }

        public void ReducePP(int amount)
        {
            PP -= amount;
        }
    }
}
