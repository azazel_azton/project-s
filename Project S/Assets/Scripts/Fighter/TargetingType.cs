﻿namespace Fighter
{
    public class TargetingType
    {
        public int TargetingTypeID { get; private set; }
        public TargetingType(int targetingTypeID)
        {
            this.TargetingTypeID = targetingTypeID;
        }

        public TargetingType(TargetingType copy)
        {
            this.TargetingTypeID = copy.TargetingTypeID;
        }

    }
}

