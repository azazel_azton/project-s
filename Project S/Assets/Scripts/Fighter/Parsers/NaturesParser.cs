﻿using Fighter;
using projectS;
using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Parsers
{
    public class NaturesParser
    {
        public NatureInfo[] ParseToNatureIntoArray(string text, InfoContainer infos)
        {
            string[] fullNatureStrings = text.Split('[');
            NatureInfo[] natures = new NatureInfo[fullNatureStrings.Length];
            for (int i = 0; i < fullNatureStrings.Length; i++)
            {
                string[] seperated = fullNatureStrings[i].Split('\n');


                string name = "";
                string beneficial = "";
                string hindering = "";

                if (seperated.Length > 1)
                {
                    for (int j = 0; j < seperated.Length; j++)
                    {
                        string[] subSep = seperated[j].Split('=');

                        if (subSep != null && subSep.Length > 0)
                        {
                            string sub = subSep[0].Trim();

                            if (sub == "Name")
                            {
                                name = seperated[j].Split('=')[1].Trim();                                
                            }
                            if (sub == "Beneficial")
                            {
                               beneficial = seperated[j].Split('=')[1].Trim();
                            }
                            if (sub == "Hindering")
                            {
                                hindering = seperated[j].Split('=')[1].Trim();
                            }
                        }
                    }
                }
                
                if (name != "")
                {
                    NatureInfo asset = ScriptableObject.CreateInstance<NatureInfo>();

                    NatureInfo.Type ben = beneficial == "Attack"? NatureInfo.Type.Attack :
                        beneficial == "Defense" ? NatureInfo.Type.Defense:
                        beneficial == "Speed" ? NatureInfo.Type.Speed:
                        beneficial == "Sp Attack" ? NatureInfo.Type.SpAttack:
                        beneficial == "Sp Defense" ? NatureInfo.Type.SpDefense:
                        NatureInfo.Type.None;
                    NatureInfo.Type hin = hindering == "Attack" ? NatureInfo.Type.Attack :
                        hindering == "Defense" ? NatureInfo.Type.Defense :
                        hindering == "Speed" ? NatureInfo.Type.Speed :
                        hindering == "Sp Attack" ? NatureInfo.Type.SpAttack :
                        hindering == "Sp Defense" ? NatureInfo.Type.SpDefense :
                        NatureInfo.Type.None;


                    asset.SetNatureInfo(name, ben, hin);
#if UNITY_EDITOR
                    AssetDatabase.CreateAsset(asset, "Assets/Resources/ScriptableObjects/Natures/" + name + ".asset");
                    AssetDatabase.SaveAssets();
#endif
                    natures[i] = asset;
                }

            }


            return natures;
        }
    }
}