﻿using Fighter;
using projectS;
using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Parsers
{
    public class FighterParser
    {
        public FighterInfo[] ParseToFighterIntoArray(string text, InfoContainer infos)
        {
            string[] fullFighterStrings = text.Split('[');
            FighterInfo[] fighters = new FighterInfo[fullFighterStrings.Length];
            for (int i=0; i<fullFighterStrings.Length; i++)
            {
                string[] seperated = fullFighterStrings[i].Split('\n');


                string name = "", genderRate, growthRate;
                string internalName = "";
                FighterType type1 = null;
                FighterType type2 = null;
                int baseExp = 0;
                Attack[] attacks = new Attack[0];
                string evolutionName = "";
                int evolutionLevel = 100;

                int hp = 0;
                int atk = 0;
                int def = 0;
                int speed = 0;
                int spatk = 0;
                int spdef = 0;

                if (seperated.Length > 1)
                {
                    for(int j=0; j<seperated.Length; j++)
                    {
                        string[] subSep = seperated[j].Split('=');

                        if (subSep != null && subSep.Length > 0)
                        {
                            string sub = subSep[0].Trim();


                            if (sub == "Name")
                            {
                                name = seperated[j].Split('=')[1].Trim();

                                Debug.Log("name : " + name);
                            }
                            else if (sub == "InternalName")
                            {
                                internalName = seperated[j].Split('=')[1].Trim();
                            }
                            else if (sub == "Type1")
                            {
                                type1 = infos.FighterTypes[seperated[j].Split('=')[1].Trim()];
                            }
                            else if (sub == "Type2")
                            {
                                type2 = infos.FighterTypes[seperated[j].Split('=')[1].Trim()];
                            }
                            else if (sub == "BaseStats")
                            {
                                hp = int.Parse(seperated[j].Split('=')[1].Split(',')[0].Trim());
                                atk = int.Parse(seperated[j].Split('=')[1].Split(',')[1].Trim());
                                def = int.Parse(seperated[j].Split('=')[1].Split(',')[2].Trim());
                                speed = int.Parse(seperated[j].Split('=')[1].Split(',')[3].Trim());
                                spatk = int.Parse(seperated[j].Split('=')[1].Split(',')[4].Trim());
                                spdef = int.Parse(seperated[j].Split('=')[1].Split(',')[5].Trim());
                            }
                            else if (sub == "GenderRate")
                            {
                                genderRate = seperated[j].Split('=')[1].Trim();
                            }
                            else if (sub == "GrowthRate")
                            {
                                growthRate = seperated[j].Split('=')[1].Trim();
                            }
                            else if (sub == "BaseEXP")
                            {
                                baseExp = int.Parse(seperated[j].Split('=')[1].Trim());
                            }
                            else if (sub == "Moves")
                            {
                                string[] seperatedMovesAndLevels = seperated[j].Trim().Split('=')[1].Split(',');
                                attacks = new Attack[(int)seperatedMovesAndLevels.Length / 2];
                                for (int a = 0; a < seperatedMovesAndLevels.Length; a += 2)
                                {
                                    if(a + 1 < seperatedMovesAndLevels.Length)
                                    {
                                        string attackName = seperatedMovesAndLevels[a + 1].Trim();
                                        try
                                        {
                                            attacks[(int)a / 2] = new Attack(infos.AttackInfos[attackName], int.Parse(seperatedMovesAndLevels[a].Trim()));
                                        }
                                        catch(Exception e)
                                        {
                                            Debug.LogError(e.ToString() + ", attack name :" + attackName + ", figher name : " + name);
                                        }
                                        
                                    }
                                }
                            }
                            else if (sub == "Evolutions")
                            {
                                if(seperated[j].Split('=')[1].Split(',').Length > 1)
                                {
                                    evolutionName = seperated[j].Split('=')[1].Split(',')[0].Trim();
                                    if(!int.TryParse(seperated[j].Split('=')[1].Split(',')[2].Trim(), out evolutionLevel))
                                    {
                                        Debug.LogWarning("Implement Use a stone : " + seperated[j].Split('=')[1].Split(',')[2].Trim());
                                    }
                                }
                            }


                            
                        }
                    }

                }

                FighterType[] types = new FighterType[type2 == null ? 1 : 2];
                types[0] = type1;
                if (type2 != null)
                {
                    types[1] = type2;
                }
                if(name != "")
                {
                    FighterInfo asset = ScriptableObject.CreateInstance<FighterInfo>();
                    asset.SetFighterInfo(internalName,name, attacks, types, hp, atk, def, spatk, spdef, speed, baseExp);
#if UNITY_EDITOR
                  AssetDatabase.CreateAsset(asset, "Assets/Resources/ScriptableObjects/Fighters/" + name + ".asset");
                  AssetDatabase.SaveAssets();
#endif
                    fighters[i] = asset;
                }

            }
                

            return fighters;
        }
    }
}