﻿using Fighter;
using projectS;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Parsers
{
    public class AttackParser
    {
        public AttackInfo[] ParseToAttackInfoArray(string text, InfoContainer infos)
        {
            string[] fullAttackInfos = text.Split('\n');
            AttackInfo[] attacks = new AttackInfo[fullAttackInfos.Length];
            for(int i=0; i<attacks.Length; i++)
            {
                string name, effectCode, damage, elementType, moveType, accuracy, pp, additionalMoveEffectChance, targetingTypeS, priority, flags;
                string description = fullAttackInfos[i].Split('\"')[1].Split('\"')[0];
                string[] seperated = fullAttackInfos[i].Split('\"')[0].Split(',');
                name = seperated[1].Trim();
                effectCode = seperated[3].Trim();
                damage = seperated[4].Trim();
                elementType = seperated[5].Trim();
                moveType = seperated[6].Trim();
                accuracy = seperated[7].Trim();
                pp = seperated[8].Trim();
                additionalMoveEffectChance = seperated[9].Trim();
                targetingTypeS = seperated[10].Trim();
                priority = seperated[11].Trim();
                flags = seperated[12].Trim();

                FighterType moveElementType = infos.FighterTypes[elementType];
                MoveEffectType moveEffectType = new MoveEffectType(moveType);
                TargetingType targetingType = new TargetingType(int.Parse(targetingTypeS));
                BattleMoveFlag[] battleFlags = new BattleMoveFlag[flags.Split(',').Length];
                for(int j=0; j<battleFlags.Length; j++)
                {
                    battleFlags[j] = new BattleMoveFlag(flags.Split(',')[j]);
                }
                if(name != "")
                {
                    AttackInfo asset = ScriptableObject.CreateInstance<AttackInfo>();
                    asset.SetAttackInfo(name, int.Parse(priority), int.Parse(damage), int.Parse(accuracy), int.Parse(pp), int.Parse(additionalMoveEffectChance), moveElementType, moveEffectType, targetingType, battleFlags, description, moveType);

#if UNITY_EDITOR
                    AssetDatabase.CreateAsset(asset, "Assets/Resources/ScriptableObjects/Attacks/" + name + ".asset");
                    AssetDatabase.SaveAssets();
#endif
                    attacks[i] = asset;
                }

            }
            return attacks;
        }
    }
}