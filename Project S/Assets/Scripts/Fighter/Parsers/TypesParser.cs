﻿using Fighter;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Parsers
{
    public class TypesParser
    {
        public FighterType[] ParseToFighterTypeArray(string text)
        {
            string[] fullTypesStrings = text.Split('[');
            FighterType[] types = new FighterType[fullTypesStrings.Length];
            for(int i=0; i<fullTypesStrings.Length; i++)
            {
                string[] seperated = fullTypesStrings[i].Split('\n');
                string name = "";
                string weaknesses = "";
                string immunities = "";
                string resistances = "";
                foreach (var s in seperated)
                {
                    if(s.Length >= 4)
                    {
                        if (s.Substring(0, 4) == "Inte")
                        {
                            name = s.Split('=')[1].Trim();
                        }
                        else if (s.Substring(0, 4) == "Weak")
                        {
                            weaknesses = s.Split('=')[1].Trim();
                        }
                        else if (s.Substring(0, 4) == "Immu")
                        {
                            immunities = s.Split('=')[1].Trim();
                        }
                        else if (s.Substring(0, 4) == "Resi")
                        {
                            resistances = s.Split('=')[1].Trim();
                        }
                    }
                    
                }

                if (name != "")
                {
                    FighterType asset = ScriptableObject.CreateInstance<FighterType>();
                    asset.SetFighterType(name, weaknesses.Split(','), resistances.Split(','), immunities.Split(','));

#if UNITY_EDITOR
                    AssetDatabase.CreateAsset(asset, "Assets/Resources/ScriptableObjects/Types/" + name + ".asset");
                    AssetDatabase.SaveAssets();
#endif

                    types[i] = asset;
                }
            }

            return types;
        }
    }
}

