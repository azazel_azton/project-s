﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackTypeS {QuickAttack, Ember, VineWhip}

[System.Serializable]
public class LevelUpAttack
{
    [SerializeField]
    int level;
    [SerializeField]
    AttackTypeS attack;
}

public class AttackScriptableObject : ScriptableObject {
    

    [SerializeField]
    public int BaseDamage;


    [SerializeField]
    LevelUpAttack evolutionInfo;

    [SerializeField]
    AttackTypeS[] relearnList;

    [SerializeField]
    LevelUpAttack[] levelUpList;


}
