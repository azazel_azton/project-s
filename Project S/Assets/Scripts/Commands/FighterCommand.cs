﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace projectS {
    public class FighterCommand : MonoBehaviour
    {
        public delegate void AttackEvent(int attackNr);
        public AttackEvent OnAttack;

        public delegate void SelectFighterEvent(int index);
        public SelectFighterEvent OnSelectFighter;

        public void Attack(int attackNr)
        {
            if (OnAttack != null)
                OnAttack(attackNr);
        }

        public void SelectFighter(int index)
        {
            if (OnSelectFighter != null)
                OnSelectFighter(index);
        }
    }
}


