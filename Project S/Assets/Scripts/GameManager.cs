﻿using projectS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace projectS
{
    public class GameManager : MonoBehaviour {

        [SerializeField]
        InfoContainer container = new InfoContainer();
        [SerializeField]
        FighterInfoView fighterInfoView1 = null;
        [SerializeField]
        FighterInfoView fighterInfoView2 = null;

        Player player1 = null;
        Player player2 = null;

        [SerializeField]
        FighterCommand fighter1Command = null;
        [SerializeField]
        FighterCommand fighter2Command = null;
        [SerializeField]
        ImageDB imageDB = null;

        [SerializeField]
        bool reloadInfo = true;

        [SerializeField]
        PhotonView photonView = null;

        private void Start()
        {
            imageDB.Init();
            container.LoadInfo(reloadInfo);
            fighter1Command.OnAttack += Fighter1Attack;
            fighter2Command.OnAttack += Fighter2Attack;
            fighter1Command.OnSelectFighter += SelectFighterPlayer1;
            fighter2Command.OnSelectFighter += SelectFighterPlayer2;


            RandomOpponents();
            UpdateViews();
        }
        private void OnDestroy()
        {
            fighter1Command.OnAttack  -= Fighter1Attack;
            fighter2Command.OnAttack -= Fighter2Attack;
            fighter1Command.OnSelectFighter -= SelectFighterPlayer1;
            fighter2Command.OnSelectFighter -= SelectFighterPlayer2;
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.R))
            {
                RandomOpponents();
                UpdateViews();
            }
        }

        public void RandomOpponents()
        {
            for(int p =0; p<2; p++)
            {
                int[] rand = new int[6];
                Fighter.Fighter[] tempFighters = new Fighter.Fighter[rand.Length];
                for (int i = 0; i < rand.Length; i++)
                {
                    rand[i] = (int)(Random.value * (container.Fighters.Keys.Count) - 0.00001f);
                }
                int ctr = 0;
                foreach (var k in container.Fighters.Keys)
                {
                    for (int i = 0; i < rand.Length; i++)
                    {
                        if (ctr == rand[i])
                        {
                            tempFighters[i] = new Fighter.Fighter(container.Fighters[k]);                       
                        }
                    }
                    ctr++;
                }
                if (p == 0)
                {
                    tempFighters[0] = new Fighter.Fighter(container.Fighters["PIKACHU"]);
                    tempFighters[1] = new Fighter.Fighter(container.Fighters["BULBASAUR"]);
                    tempFighters[2] = new Fighter.Fighter(container.Fighters["SQUIRTLE"]);
                    tempFighters[3] = new Fighter.Fighter(container.Fighters["CHARMANDER"]);
                    tempFighters[4] = new Fighter.Fighter(container.Fighters["ONIX"]);
                    tempFighters[5] = new Fighter.Fighter(container.Fighters["GASTLY"]);
                    tempFighters[0].SetNature(container.Natures["Modest"]);
                    tempFighters[1].SetNature(container.Natures["Lonely"]);
                    tempFighters[2].SetNature(container.Natures["Brave"]);
                    tempFighters[3].SetNature(container.Natures["Adamant"]);
                    tempFighters[4].SetNature(container.Natures["Naughty"]);
                    tempFighters[5].SetNature(container.Natures["Bold"]);

                    tempFighters[0].PersonalInfo.Level = 50;
                    tempFighters[1].PersonalInfo.Level = 50;
                    tempFighters[2].PersonalInfo.Level = 50;
                    tempFighters[3].PersonalInfo.Level = 50;
                    tempFighters[4].PersonalInfo.Level = 50;
                    tempFighters[5].PersonalInfo.Level = 50;
                    player1 = new Player(tempFighters);
                }
                else
                {
                    tempFighters[1] = new Fighter.Fighter(container.Fighters["PIKACHU"]);
                    tempFighters[0] = new Fighter.Fighter(container.Fighters["BULBASAUR"]);
                    tempFighters[2] = new Fighter.Fighter(container.Fighters["SQUIRTLE"]);
                    tempFighters[3] = new Fighter.Fighter(container.Fighters["CHARMANDER"]);
                    tempFighters[4] = new Fighter.Fighter(container.Fighters["ONIX"]);
                    tempFighters[5] = new Fighter.Fighter(container.Fighters["GASTLY"]);
                    tempFighters[1].SetNature(container.Natures["Naughty"]);
                    tempFighters[0].SetNature(container.Natures["Relaxed"]);
                    tempFighters[2].SetNature(container.Natures["Impish"]);
                    tempFighters[3].SetNature(container.Natures["Lax"]);
                    tempFighters[4].SetNature(container.Natures["Timid"]);
                    tempFighters[5].SetNature(container.Natures["Hasty"]);

                    tempFighters[0].PersonalInfo.Level = 50;
                    tempFighters[1].PersonalInfo.Level = 50;
                    tempFighters[2].PersonalInfo.Level = 50;
                    tempFighters[3].PersonalInfo.Level = 50;
                    tempFighters[4].PersonalInfo.Level = 50;
                    tempFighters[5].PersonalInfo.Level = 50;
                    player2 = new Player(tempFighters);
                }
            }
            

            UpdateViews();
        }

        private void UpdateViews()
        {
            fighterInfoView1.ShowInfo(player1, player2);
            fighterInfoView2.ShowInfo(player2, player1);
        }

        public void Fighter1Attack(int attackNr)
        {
            if(player1.CurrentFighter.PersonalInfo.Health > 0 && player1.CurrentFighter.PersonalInfo.Attacks[attackNr].Info.PP > 0)
            {
                Attack(player1.CurrentFighter, player2.CurrentFighter, attackNr);
                //photonView.RPC("SendAttack", PhotonTargets.Others, new object[] { attackNr });
            }
        }
        
        public void Fighter2Attack(int attackNr)
        {
            if (player2.CurrentFighter.PersonalInfo.Health > 0 && player2.CurrentFighter.PersonalInfo.Attacks[attackNr].Info.PP > 0)
            {
                Attack(player2.CurrentFighter, player1.CurrentFighter, attackNr);
            }
        }

        private void Attack(Fighter.Fighter attacker, Fighter.Fighter defender, int attackNr)
        {
            float modifier = 1.0f;
            foreach (var type in defender.PersonalInfo.Types)
            {
                modifier *= type.StrengthAgainst(attacker.PersonalInfo.Attacks[attackNr].Info.ElementType);
            }
            foreach (var type in attacker.PersonalInfo.Types)
            {
                if (type.TypeName == attacker.PersonalInfo.Attacks[attackNr].Info.ElementType.TypeName)
                {
                    modifier *= 1.5f;
                }
            }
            float attack = attacker.PersonalInfo.Attacks[attackNr].Info.MoveType == "Physical" ? attacker.PersonalInfo.UseValue(attacker.PersonalInfo.Attack, Fighter.NatureInfo.Type.Attack) :
                           attacker.PersonalInfo.Attacks[attackNr].Info.MoveType == "Special" ? attacker.PersonalInfo.UseValue(attacker.PersonalInfo.SPAttack, Fighter.NatureInfo.Type.SpAttack) : 1;
            float defense = attacker.PersonalInfo.Attacks[attackNr].Info.MoveType == "Physical" ? defender.PersonalInfo.UseValue(defender.PersonalInfo.Defense, Fighter.NatureInfo.Type.Defense) :
                          attacker.PersonalInfo.Attacks[attackNr].Info.MoveType == "Special" ? defender.PersonalInfo.UseValue(defender.PersonalInfo.SPDefense, Fighter.NatureInfo.Type.SpDefense) : 1;

            if (attacker.PersonalInfo.Attacks[attackNr].Info.MoveType == "Status")
            {
                modifier = 0;

            }

            float damage = (((((((2 * attacker.PersonalInfo.Level) / 5.0f) + 2) * attacker.PersonalInfo.Attacks[attackNr].Info.Damage * attack / defense) / 50.0f) + 2) * modifier) * (0.85f + Random.value * 0.15f);
            defender.PersonalInfo.DecreaseHealth((int)damage);
            attacker.PersonalInfo.Attacks[attackNr].Info.ReducePP(1);
            UpdateViews();
        }
        
        public void SelectFighterPlayer1(int index)
        {
            player1.SelectFighter(index);
            UpdateViews();
            //photonView.RPC("SendFighterSelection", PhotonTargets.Others, new object[] { index });
        }

        public void SelectFighterPlayer2(int index)
        {
            player2.SelectFighter(index);
            UpdateViews();

        }

        [PunRPC]
        public void SendFighterSelection(int index)
        {
            SelectFighterPlayer2(index);
        }
        [PunRPC]
        public void SendAttack(int attackNr)
        {
            Fighter2Attack(attackNr);
        }


    }

}
