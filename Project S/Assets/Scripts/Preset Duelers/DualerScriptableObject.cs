﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fighter;

namespace projectS
{
    public class DualerScriptableObject : ScriptableObject
    {
        [SerializeField]
        Fighter.FighterInfo[] fighters;

    }

}
