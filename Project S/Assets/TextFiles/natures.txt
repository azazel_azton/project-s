[0]
Name=Hardy
Beneficial=NONE
Hindering=NONE
[1]
Name=Lonely
Beneficial=Attack
Hindering=Defense
[2]
Name=Brave
Beneficial=Attack		
Hindering=Speed
[3]
Name=Adamant
Beneficial=Attack
Hindering=Sp Attack
[4]
Name=Naughty
Beneficial=Attack
Hindering=Sp Defense
[5]
Name=Bold
Beneficial=Defense
Hindering=Attack
[6]
Name=Docile
Beneficial=NONE
Hindering=NONE
[7]
Name=Relaxed
Beneficial=Defense
Hindering=Speed
[8]
Name=Impish
Beneficial=Defense
Hindering=Sp Attack
[9]
Name=Lax
Beneficial=Defense
Hindering=Sp Defense
[10]
Name=Timid
Beneficial=Speed
Hindering=Attack
[11]
Name=Hasty
Beneficial=Speed
Hindering=Defense
[12]
Name=Serious
Beneficial=NONE
Hindering=NONE
[13]
Name=Jolly
Beneficial=Speed
Hindering=Sp Attack
[14]
Name=Naive
Beneficial=Speed
Hindering=Sp Defense
[15]
Name=Modest
Beneficial=Sp Attack
Hindering=Attack
[16]
Name=Mild
Beneficial=Sp Attack
Hindering=Defense
[17]
Name=Quiet
Beneficial=Sp Attack
Hindering=Speed
[18]
Name=Bashful
Beneficial=NONE
Hindering=NONE
[19]
Name=Rash
Beneficial=Sp Attack
Hindering=Sp Defense
[20]
Name=Calm
Beneficial=Sp Defense
Hindering=Attack
[21]
Name=Gentle
Beneficial=Sp Defense
Hindering=Defense
[22]
Name=Sassy
Beneficial=Sp Defense
Hindering=Speed
[23]
Name=Careful
Beneficial=Sp Defense
Hindering=Sp Attack
[24]
Name=Quirky
Beneficial=NONE
Hindering=NONE
